# Ymu-CLI
A CLI client for [ymu-server](https://gitlab.com/OptimalStrategy/ymusic-player/).

## Development
1. Clone the repository
    ```bash
    $ git clone git@gitlab.com:OptimalStrategy/ymu-cli.git && cd ymu-cli
    ```

2. Install Rust via your package manager or https://rustup.rs/. The client has been tested on `rustc 1.40.0-nightly`.
    ```bash
    $ rustup default nightly
    $ rustup update
    $ rustc --version
    rustc 1.40.0-nightly (0e8a4b441 2019-10-16)
    ```

3. Build the binary.
    ```bash
    $ cargo install --path .
    ...
      Finished release [optimized] target(s) in 56.22s
    Installing /home/<user>/.cargo/bin/ymu
    ```

    If you don't have the cargo bin directory on PATH, add it. Replace `SHELL_CONFIG` with the path 
    to the config file of your shell, then execute the command below.
    ```bash
    $ export SHELL_CONFIG="$HOME/.zshrc"  # replace with your shell config
    $ echo 'export PATH=$PATH:~/.cargo/bin' >> $SHELL_CONFIG
    ```

All done! The client is ready:

```bash
$ ymu -h
Ymu CLI client 0.1.4
Provides a command line interface to ymu-server.

USAGE:
    ymu [FLAGS] [OPTIONS] [SUBCOMMAND]

FLAGS:
    -d, --debug      Print raw metadata
    -h, --help       Prints help information
    -s, --status     Display the track information & player status (this option will be executed the last)
    -V, --version    Prints version information

OPTIONS:
    -c, --config <FILE>        Path to the config [default: /home/george/.yplayer/yplayer.json]
    -t, --timeout <timeout>    Attempt to connect to the server within this number of seconds [default: 30]

SUBCOMMANDS:
    album       Play an album
    back        Skip back to the previous track. This option does not work when playing a radio.
    cover       Print the path to the temporary stored album cover
    history     Display the list of all songs played during the session
    like        Like or dislike the track
    pause       Pause the track
    play        Resume playing the track
    playlist    Play a playlist
    quality     Set the sound quality
    radio       Play a radio
    shutdown    Shutdown the server
    skip        Skip the track
    song        Print the currently playing track
    track       Play a track
    volume      Manage the volume
```
Use `-s` to display a status message after executing a command:
```bash
$ ymu -s like
Added 'A Castle on the Sea — Aviators' to favorites.
Song: A Castle on the Sea - Aviators [1.82%]
Status: 🎶 Playing
Like status: 💖 Liked
Audio kind: 📻 Radio
Quality: HQ
Volume: 100
Song link: https://music.yandex.com/album/6714875/track/49088537
```
Shutdown the server:
```bash
$ ymu shutdown
Sending the shutdown request...
# Server logs:
2019-02-15 14:20:12.364 | DEBUG    | ymusic.yserver:shutdown:300 - Received shutdown request.
2019-02-15 14:20:12.445 | DEBUG    | ymusic.driver:graceful_shutdown:231 - Saved cookies.
2019-02-15 14:20:12.445 | DEBUG    | ymusic.driver:close_all:198 - Closing windows...
2019-02-15 14:20:12.452 | DEBUG    | ymusic.driver:close_all:201 - Closing Yandex Music — radio for every taste
2019-02-15 14:20:13.049 | DEBUG    | ymusic.driver:close_all:206 - Shutting down the driver...
2019-02-15 14:20:13.085 | DEBUG    | ymusic.driver:close_all:208 - Done.
2019-02-15 14:20:13.085 | DEBUG    | ymusic.config:_clear_tmp:78 - Cleaning tmp directory.
```
