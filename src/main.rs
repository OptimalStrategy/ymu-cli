#[cfg(feature = "tui")]
extern crate cursive;

extern crate clap;
extern crate dirs;
extern crate ymusic_api_wrapper;

use clap::{App, AppSettings, Arg, SubCommand};
use std::{fs, str::FromStr};
use ymusic_api_wrapper::{networking::LikeResult, yapi::YAPI, yconfig};

#[cfg(feature = "tui")]
mod ui;

mod utils;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let default_config_path =
        fs::canonicalize(dirs::home_dir().unwrap().join(".yplayer/yplayer.json"));

    if default_config_path.is_err() {
        eprintln!(
            "Missing the default config file: '~/.yplayer/yplayer.json'. \
             Please run the server first; that will generate the required files."
        );
        ::std::process::exit(2);
    }
    let default_config = default_config_path.unwrap();

    let app = App::new("Ymu CLI client")
        .version(env!("CARGO_PKG_VERSION"))
        .about("Provides a command line interface to ymu-server.")
        .setting(AppSettings::DisableHelpSubcommand)
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("Path to the config")
                .default_value(&default_config.to_str().unwrap())
                .takes_value(true),
        )
        .arg(
            Arg::with_name("timeout")
                .short("t")
                .long("timeout")
                .takes_value(true)
                .validator(|v| match i8::from_str(&v.as_str()) {
                    Err(_) => Err(String::from("Expected a number of seconds")),
                    _ => Ok(()),
                })
                .default_value("30")
                .help("Attempt to connect to the server within this number of seconds"),
        )
        .arg(
            Arg::with_name("debug")
                .short("d")
                .long("debug")
                .help("Print raw metadata"),
        )
        .arg(Arg::with_name("status").short("s").long("status").help(
            "Display the track information & player status (this option will be executed the last)",
        ))
        .subcommand(SubCommand::with_name("play").about("Resume playing the track"))
        .subcommand(SubCommand::with_name("pause").about("Pause the track"))
        .subcommand(SubCommand::with_name("like").about("Like or dislike the track"))
        .subcommand(SubCommand::with_name("song").about("Print the currently playing track"))
        .subcommand(SubCommand::with_name("skip").about("Skip the track"))
        .subcommand(SubCommand::with_name("back").about(
            "Skip back to the previous track. This option does not work when playing a radio.",
        ))
        .subcommand(
            SubCommand::with_name("cover")
                .about("Print the path to the temporary stored album cover"),
        )
        .subcommand(
            SubCommand::with_name("history")
                .about("Display the list of all songs played during the session"),
        )
        .subcommand(SubCommand::with_name("shutdown").about("Shutdown the server"))
        .subcommand(
            SubCommand::with_name("volume")
                .about("Manage the volume")
                .arg(
                    Arg::with_name("value")
                        .value_name("0..100")
                        .validator(|v| match u8::from_str(&v.as_str()) {
                            Ok(d) if d <= 100 => Ok(()),
                            Err(_) | Ok(_) => {
                                Err(String::from("Expected an integer from 0 to 100"))
                            }
                        })
                        .help("Change the volume"),
                ),
        )
        .subcommand(
            SubCommand::with_name("track").about("Play a track").arg(
                Arg::with_name("track")
                    .required(true)
                    .value_name("URL")
                    .help("Track link"),
            ),
        )
        .subcommand(
            SubCommand::with_name("album").about("Play an album").arg(
                Arg::with_name("album")
                    .required(true)
                    .value_name("URL or ID")
                    .help("Album link or ID."),
            ),
        )
        .subcommand(
            SubCommand::with_name("playlist")
                .about("Play a playlist")
                .arg(
                    Arg::with_name("playlist")
                        .required(true)
                        .value_name("URL")
                        .help("Playlist url"),
                ),
        )
        .subcommand(
            SubCommand::with_name("radio").about("Play a radio").arg(
                Arg::with_name("index")
                    .required(true)
                    .validator(|v| match u8::from_str(&v.as_str()) {
                        Err(_) => Err(String::from("Expected an integer")),
                        _ => Ok(()),
                    })
                    .help("Radio index (1-based)")
                    .default_value("1"),
            ),
        )
        .subcommand(
            SubCommand::with_name("quality")
                .about("Set the sound quality")
                .arg(
                    Arg::with_name("type")
                        .possible_values(&["high", "low"])
                        .help("Quality type")
                        .required(true),
                ),
        );
    let matches = app.get_matches();

    let config: yconfig::YConfig;
    if let Some(value) = matches.value_of("config") {
        config = yconfig::YConfig::from_config(value)?
    } else {
        config = yconfig::YConfig::default()
    };

    let api: YAPI = YAPI::new(config);
    let timeout = i8::from_str(matches.value_of("timeout").unwrap())?;

    // Attempt to connect to the server
    if api.check_server_status().is_down() {
        let connected = utils::wait_for_server(&api, Some(timeout))?;
        if !connected {
            println!("Failed to connect to the server.")
        }
    } else if matches.subcommand_matches("play").is_some() {
        if api.play()? {
            println!("Resumed playing.")
        } else {
            println!("Unable to resume playing. Most likely, the track is already playing.")
        }
    } else if matches.subcommand_matches("pause").is_some() {
        if api.pause()? {
            println!("Paused the track.")
        } else {
            println!("Unable to pause the track. Most likely, the track is already paused.");
        }
    } else if matches.subcommand_matches("like").is_some() {
        match api.like()? {
            LikeResult::ADDED => println!("Added '{}' to favorites.", api.song()?),
            LikeResult::REMOVED => println!("Removed '{}' from favorites.", api.song()?),
            LikeResult::FAIL => {
                println!("Failed to change the track's status. See server logs for more details.")
            }
        }
    } else if matches.subcommand_matches("song").is_some() {
        println!("{}", api.song()?);
    } else if matches.subcommand_matches("skip").is_some() {
        if api.skip()? {
            println!("Skipped the track.");
        } else {
            println!("Unable to skip the track. Please check the server logs for more info.");
        }
    } else if matches.subcommand_matches("back").is_some() {
        if api.back()? {
            println!("Returned to the previous track");
        } else {
            println!(
                "Unable to return to the previous track. \
                 Please check that you are not listening in the radio mode, otherwise, see the server logs for more info."
            )
        }
    } else if let Some(matches) = matches.subcommand_matches("volume") {
        if let Some(volume) = matches.value_of("value") {
            println!(
                "Updated volume: {}",
                api.volume(Some(u8::from_str(&volume).unwrap()))?
            );
        } else {
            println!("Volume: {}", api.volume(None)?);
        }
    } else if let Some(matches) = matches.subcommand_matches("track") {
        let r = api.track(matches.value_of("track").unwrap())?;
        match r {
            Ok(track) => println!("Playing {}", track),
            Err(err) => eprintln!("Unable to play the track: {}", err),
        };
    } else if let Some(matches) = matches.subcommand_matches("album") {
        let album = matches.value_of("album").unwrap();
        let r = match usize::from_str(album) {
            Ok(id) => api.album_by_id(id),
            Err(_) => api.album_by_link(album),
        }?;
        match r {
            Ok(track) => println!("Playing {}", track),
            Err(err) => eprintln!("Unable to play the album: {}", err),
        };
    } else if let Some(matches) = matches.subcommand_matches("playlist") {
        let r = api.playlist(matches.value_of("playlist").unwrap())?;
        match r {
            Ok(track) => println!("Playing {}", track),
            Err(err) => eprintln!("Unable to play the playlist: {}", err),
        };
    } else if let Some(matches) = matches.subcommand_matches("radio") {
        let index = matches.value_of("radio").unwrap_or("1");
        let r = api.radio(u8::from_str(index).unwrap())?;
        match r {
            Ok(track) => println!("Playing {}", track),
            Err(err) => eprintln!("Unable to play the radio: {}", err),
        };
    } else if let Some(matches) = matches.subcommand_matches("quality") {
        let quality = matches.value_of("type").unwrap();
        let res = match quality {
            "high" => api.hq_on()?,
            _ => api.hq_off()?,
        };

        if res {
            println!("Changed sound quality to {}.", quality)
        } else {
            println!(
                "Failed to change sound quality. \
                 Most likely, either the quality is already '{}' or you are not a paid subscriber.",
                quality
            )
        }
    } else if matches.subcommand_matches("cover").is_some() {
        let path = api.cover()?;
        println!("Path to the cover: {}", path)
    } else if matches.subcommand_matches("history").is_some() {
        api.song_history()?
            .iter()
            .enumerate()
            .for_each(|(i, s)| println!("{}. {}", i + 1, s));
    } else if matches.subcommand_matches("shutdown").is_some() {
        println!("Sending a shutdown request...");
        api.shutdown();
    }

    if matches.subcommand.is_none() && !matches.is_present("status") {
        println!("The server at {} is up.", api.url());
    } else if matches.is_present("status") && matches.subcommand_matches("shutdown").is_none() {
        if matches.is_present("debug") {
            println!("{:#?}", api.status()?);
        } else {
            println!("{}", api.status()?);
        }
    }

    Ok(())
}
