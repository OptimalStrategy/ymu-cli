extern crate colored;

use colored::*;
use serde::Deserialize;
use std::fmt;

#[derive(Debug)]
pub enum ServerStatus {
    Down,
    Auth,
    Up,
}

// TODO: Stuff this enum insde YServerStatusData
#[derive(Debug)]
pub enum LikeResult {
    FAIL = 0,
    REMOVED = 1,
    ADDED = 2,
}

impl ServerStatus {
    pub fn is_down(&self) -> bool {
        match &self {
            ServerStatus::Down => true,
            _ => false,
        }
    }
}

#[derive(Deserialize, Debug)]
pub struct YResponseJson {
    pub status: String,
    pub data: Option<YResponseData>,
}

#[derive(Deserialize, Debug)]
pub struct YResponseData {
    pub error: Option<String>,
    pub error_meta: Option<String>,
    pub song: Option<String>,
    pub favorite: Option<String>,
    pub volume: Option<f32>,
    pub cover: Option<String>,
    pub history: Option<Vec<String>>,
}

#[derive(Deserialize, Debug)]
pub struct YServerStatusJson {
    pub status: String,
    pub data: YServerStatusData,
}

#[derive(Deserialize, Debug)]
pub struct YServerStatusData {
    pub artist: String,
    pub default_title: String,
    pub favorite: String,
    pub kind: String,
    pub playing: bool,
    pub progress: f64,
    pub quality: String,
    pub song_url: String,
    pub station: String,
    pub title: String,
    pub volume: f32,
}
impl fmt::Display for YServerStatusData {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}: {} - {} [{}%]\n\
             {}: {}\n\
             {}: {}\n\
             {}: {}\n\
             {}: {}\n\
             {}: {}\n\
             {}: {}",
            // First row
            "Song".green().underline(),
            self.title,
            self.artist,
            format!("{:.2}", self.progress).green(),
            // Second row
            "Status".green().underline(),
            if self.playing {
                "\u{1F3B6} Playing".blue() // The music notes emoji
            } else {
                "\u{23F8} Paused".cyan() // Pause emoji
            },
            // Third row
            "Like status".green().underline(),
            match &self.favorite[..] {
                "LikeResult.ADDED" => String::from("\u{1F496} Liked").magenta(), // Sparklinkg Heart emloji
                _ => String::from("\u{1F494} Disliked").red(), // Broken heart emoji
            },
            // Fourth row
            "Audio kind".green().underline(),
            (match &self.kind[..] {
                "ContentKind.RADIO" => "\u{1F4FB} Radio",
                "ContentKind.ALBUM" => "\u{1F4BF} Album",
                "ContentKind.PLAYLIST" => "\u{1F5BC} Playlist",
                _ => "Unknown",
            })
            .bright_blue(),
            // Fith row
            "Quality".green().underline(),
            if self.quality == "HQ" {
                self.quality.yellow()
            } else {
                self.quality.blue()
            },
            // Sixth row
            "Volume".green().underline(),
            format!("{}", self.volume).blue(),
            // Seventh row
            "Song link".green().underline(),
            self.song_url.magenta().underline(),
        )
    }
}
