extern crate reqwest;
extern crate serde;

use reqwest::Client;
use reqwest::StatusCode;

use crate::networking::*;
pub use crate::yconfig::YConfig;

pub struct YAPI {
    client: Client,
    url_: String,
    pub config: YConfig,
}

#[derive(Debug)]
pub struct PlayError {
    error: String,
    meta: String,
}

type APICall<T> = Result<T, Box<dyn std::error::Error>>;

impl YAPI {
    pub fn new(config: YConfig) -> YAPI {
        let url_ = config.url();
        YAPI {
            client: Client::new(),
            config,
            url_,
        }
    }

    pub fn url(&self) -> &String {
        &self.url_
    }

    fn endpoint(&self, path: &str) -> String {
        format!("{}/{}", self.url_, path)
    }

    fn get(&self, endpoint: &str) -> APICall<reqwest::Response> {
        Ok(self.client.get(self.endpoint(endpoint).as_str()).send()?)
    }

    fn with_query(&self, endpoint: &str) -> reqwest::RequestBuilder {
        self.client.get(self.endpoint(endpoint).as_str())
    }

    fn binary_result_get(&self, endpoint: &str) -> APICall<bool> {
        let mut r = self.get(&endpoint)?;
        let j: YResponseJson = r.json()?;
        Ok(match j.status.as_str() {
            "Ok" => true,
            _ => false,
        })
    }

    pub fn heartbeat(&self) -> APICall<StatusCode> {
        let r = self.get("__heartbeat__")?;
        Ok(r.status())
    }

    pub fn volume(&self, value: Option<u8>) -> APICall<u8> {
        let mut r = match value {
            Some(v) => self
                .with_query("volume")
                .query(&[("value", &v.to_string()[..])])
                .send()?,
            None => self.get("volume")?,
        };
        let j: YResponseJson = r.json()?;

        Ok(j.data.unwrap().volume.unwrap() as u8)
    }

    fn _generic_play(
        &self,
        endpoint: &str,
        query: &str,
        value: &str,
    ) -> APICall<Result<String, PlayError>> {
        let mut r = self.with_query(endpoint).query(&[(query, value)]).send()?;

        Ok(match r.status() {
            StatusCode::OK => {
                let j: YServerStatusJson = r.json()?;
                let data = j.data;
                Ok(format!("{} - {}", data.title, data.artist))
            }
            _ => {
                let j: YResponseJson = r.json()?;
                let data = j.data.unwrap();
                Err(PlayError {
                    error: data.error.unwrap(),
                    meta: data.error_meta.unwrap(),
                })
            }
        })
    }

    pub fn track(&self, value: &str) -> APICall<Result<String, PlayError>> {
        self._generic_play("play_track", "track", value)
    }

    pub fn album_by_link(&self, value: &str) -> APICall<Result<String, PlayError>> {
        self._generic_play("play_album", "link", value)
    }

    pub fn album_by_id(&self, value: usize) -> APICall<Result<String, PlayError>> {
        self._generic_play("play_album", "id", &value.to_string()[..])
    }

    pub fn playlist(&self, value: &str) -> APICall<Result<String, PlayError>> {
        self._generic_play("play_playlist", "playlist", value)
    }

    pub fn radio(&self, index: u8) -> APICall<Result<String, PlayError>> {
        self._generic_play("play_radio", "index", &index.to_string()[..])
    }

    pub fn status(&self) -> APICall<YServerStatusData> {
        let mut r = self.get("status")?;
        let j: YServerStatusJson = r.json()?;
        Ok(j.data)
    }

    pub fn hq_on(&self) -> APICall<bool> {
        self.binary_result_get("hq_on")
    }

    pub fn hq_off(&self) -> APICall<bool> {
        self.binary_result_get("hq_off")
    }

    pub fn play(&self) -> APICall<bool> {
        self.binary_result_get("play")
    }

    pub fn pause(&self) -> APICall<bool> {
        self.binary_result_get("pause")
    }

    pub fn like(&self) -> APICall<LikeResult> {
        let mut r = self.get("like")?;
        let j: YResponseJson = r.json()?;
        let liked = j.data.unwrap().favorite.unwrap();
        Ok(match &liked[..] {
            "LikeResult.ADDED" => LikeResult::ADDED,
            "LikeResult.REMOVED" => LikeResult::REMOVED,
            _ => LikeResult::FAIL,
        })
    }

    pub fn skip(&self) -> APICall<bool> {
        self.binary_result_get("skip")
    }

    pub fn back(&self) -> APICall<bool> {
        self.binary_result_get("back")
    }

    pub fn song(&self) -> APICall<String> {
        let mut r = self.get("song")?;
        let j: YResponseJson = r.json()?;
        Ok(j.data.unwrap().song.unwrap())
    }

    pub fn cover(&self) -> APICall<String> {
        let mut r = self.get("cover")?;
        let j: YResponseJson = r.json()?;
        Ok(j.data.unwrap().cover.unwrap())
    }

    pub fn song_history(&self) -> APICall<Vec<String>> {
        let mut r = self.get("song_history")?;
        let j: YResponseJson = r.json()?;
        Ok(j.data.unwrap().history.unwrap())
    }

    #[allow(unused_must_use)]
    pub fn shutdown(&self) {
        // Unusued intentionally because the server does not
        // respond after receiving a shutdown signal
        self.get("shutdown");
    }

    pub fn check_server_status(&self) -> ServerStatus {
        let code = self.heartbeat();
        if code.is_err() {
            ServerStatus::Down
        } else {
            match code.unwrap() {
                StatusCode::OK => ServerStatus::Up,
                _ => ServerStatus::Auth,
            }
        }
    }
}

impl std::fmt::Display for PlayError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{} [offender = `{}`]", self.error, self.meta)
    }
}
