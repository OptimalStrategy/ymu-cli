use serde::Deserialize;
use serde_json;
use std::fs;

pub struct YConfig {
    pub host: String,
    pub port: usize,
}

#[derive(Deserialize)]
struct YPlayerJson {
    server: Option<YServerData>,
}

#[derive(Deserialize)]
struct YServerData {
    host: String,
    port: usize,
}

impl YConfig {
    pub fn new(host: &'static str, port: usize) -> YConfig {
        YConfig {
            host: String::from(host),
            port,
        }
    }
    pub fn default() -> YConfig {
        YConfig {
            host: String::from("localhost"),
            port: 7447,
        }
    }
    pub fn from_config(config: &str) -> Result<YConfig, Box<dyn std::error::Error>> {
        let f = fs::File::open(config)?;
        let json: YPlayerJson = serde_json::from_reader(f)?;
        let conf = json.server;
        if let Some(conf) = conf {
            Ok(YConfig {
                host: conf.host,
                port: conf.port,
            })
        } else {
            eprintln!("The config is missing the 'server' entry.");
            std::process::exit(-1);
        }
    }
    pub fn url(&self) -> String {
        format!("http://{}:{}", self.host, self.port)
    }
}
