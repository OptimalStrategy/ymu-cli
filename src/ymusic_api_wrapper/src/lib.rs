pub mod networking;
pub mod yapi;
pub mod yconfig;

#[cfg(test)]
mod tests {
    use crate::networking::ServerStatus;
    use crate::yapi;
    use crate::yconfig::YConfig;
    use std::panic;

    fn get_api() -> yapi::YAPI {
        yapi::YAPI::new(YConfig::default())
    }

    #[test]
    fn sample() {
        run_test(|| {
            check_server_is_up();
        });
    }

    fn check_server_is_up() -> bool {
        let api = get_api();
        match api.check_server_status() {
            ServerStatus::Up => true,
            _ => false,
        }
    }

    fn run_test<T>(test: T) -> ()
    where
        T: FnOnce() -> () + panic::UnwindSafe,
    {
        if !check_server_is_up() {
            assert!(
                false,
                "Unable to test: The server is not ready. \
                 Please launch the server and complete the authentication if needed."
            );
        }
        let result = panic::catch_unwind(|| test());
        assert!(result.is_ok());
    }
}
