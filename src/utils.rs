extern crate chrono;

use self::chrono::Local;
use std::io::{self, Write};

use ymusic_api_wrapper::networking::ServerStatus;
use ymusic_api_wrapper::yapi::YAPI;

fn _time() -> i64 {
    Local::now().timestamp()
}

pub fn wait_for_server(
    api: &YAPI,
    timeout: Option<i8>,
) -> Result<bool, Box<dyn std::error::Error>> {
    let timeout = timeout.unwrap_or(30_i8);

    if timeout <= 0 {
        return Ok(true);
    }

    let start = _time();
    let mut end = _time();
    let mut last_digit = end % 10;
    let mut dots = 0_u8;
    let mut result: bool = false;

    while end - start < i64::from(timeout) {
        dots += (end % 10 != last_digit) as u8;
        dots %= 4;
        last_digit = end % 10;
        show_progress(
            &api.url().as_str(),
            end - start,
                i64::from(timeout),
            dots,
            3,
        )?;

        let status = api.check_server_status();
        if !status.is_down() {
            end_progress(end - start, status)?;
            result = true;
            break;
        };

        end = _time();
    }

    let stdout = io::stdout();
    let mut stdout = stdout.lock();
    write!(stdout, "\n")?;
    stdout.flush()?;

    Ok(result)
}

fn show_progress(
    url: &str,
    current_time: i64,
    max_time: i64,
    n_dots: u8,
    max_dots: u8,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut dots = ".".repeat(n_dots as usize);
    dots.push_str(" ".repeat((max_dots - n_dots) as usize).as_str());

    let stdout = io::stdout();
    let mut stdout = stdout.lock();

    write!(
        stdout,
        "\rConnecting to {} [{}] {:.2}s / {:.2}s",
        url, dots, current_time, max_time
    )?;
    stdout.flush()?;

    Ok(())
}

fn end_progress(final_time: i64, status: ServerStatus) -> Result<(), Box<dyn std::error::Error>> {
    let server = match status {
        ServerStatus::Auth => "authentication server",
        _ => "player server",
    };

    let stdout = io::stdout();
    let mut stdout = stdout.lock();
    write!(stdout, "\r\n")?;
    write!(stdout, "Connected to {} in {:.2}s", server, final_time)?;

    Ok(())
}
