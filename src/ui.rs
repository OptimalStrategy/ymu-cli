use cursive::align::HAlign;
use cursive::traits::*;
use cursive::views::{Button, Dialog, DummyView, LinearLayout, TextView};
use cursive::Cursive;
use std::cell::RefCell;
use std::rc::Rc;

use ymusic_api_wrapper::yapi::YAPI;

type API = Rc<RefCell<YAPI>>;

pub fn build_ui(siv: &mut Cursive, mut api: API) {
    siv.add_layer(
        Dialog::text("Hello!\nPress <Show> to display the status.")
            .title("Server status")
            .button("Show", move |s| show_next(s, api.clone())),
    );
}

fn show_next(s: &mut Cursive, api: API) {
    s.pop_layer();

    let r = api.borrow().status();
    let text = if r.is_ok() {
        format!("{}", r.unwrap())
    } else {
        String::from("An error has occurred")
    };
    s.add_layer(
        Dialog::text(text)
            .title("Status")
            .button("Quit", |s| s.quit()),
    );
}
